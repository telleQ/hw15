﻿using NUnit.Framework;
using RestSharp;
using HW15.Tests.DataModels;

namespace HW15.Tests
{
    [TestFixture]
    public class TodosTests
    {
        [Test]
        public static void GetSecondTodos()
        {
            RestApiHelper<Todos> restApi = new RestApiHelper<Todos>();
            var restUrl = restApi.SetUrl("/todos/2");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Todos content = restApi.GetContent<Todos>(response);
            Assert.AreEqual("1", content.UserId);
            Assert.AreEqual("2", content.Id);
            Assert.AreEqual("quis ut nam facilis et officia qui", content.Title);
            Assert.AreEqual("false", content.Completed);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void AddRecordInTodos()
        {
            string jsonString = @"{
                ""userId"": ""10"",
                ""id"": ""201"",
                ""title"": ""quis ut nam facilis et officia qui"",
                ""completed"": ""false""
              }";
            RestApiHelper<Todos> restApi = new RestApiHelper<Todos>();
            var restUrl = restApi.SetUrl("/todos");
            var restRequest = restApi.CreatePostRequest(jsonString);
            var response = restApi.GetResponse(restUrl, restRequest);
            Todos content = restApi.GetContent<Todos>(response);
            Assert.AreEqual("10", content.UserId);
            Assert.AreEqual("201", content.Id);
            Assert.AreEqual("quis ut nam facilis et officia qui", content.Title);
            Assert.AreEqual("false", content.Completed);
            Assert.AreEqual("Created", response.StatusCode.ToString());
        }

        [Test]
        public static void UpdateFirstTodos()
        {
            string jsonString = @"{
                ""userId"": ""1"",
                ""id"": ""1"",
                ""title"": ""delectus aut autem"",
                ""completed"": ""false""
              }";
            RestApiHelper<Todos> restApi = new RestApiHelper<Todos>();
            var restUrl = restApi.SetUrl("/todos/1");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Todos content = restApi.GetContent<Todos>(response);
            Assert.AreEqual("1", content.UserId);
            Assert.AreEqual("1", content.Id);
            Assert.AreEqual("delectus aut autem", content.Title);
            Assert.AreEqual("false", content.Completed);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void PatchFirstTodos()
        {
            string jsonString = @"{
                ""title"": ""Patching Todos"",
                ""completed"": ""true""
              }";
            RestApiHelper<Todos> restApi = new RestApiHelper<Todos>();
            var restUrl = restApi.SetUrl("/todos/1");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Todos content = restApi.GetContent<Todos>(response);
            Assert.AreEqual("1", content.UserId);
            Assert.AreEqual("1", content.Id);
            Assert.AreEqual("Patching Todos", content.Title);
            Assert.AreEqual("true", content.Completed);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void DeleteFirstTodos()
        {
            RestApiHelper<Todos> restApi = new RestApiHelper<Todos>();
            var restUrl = restApi.SetUrl("/todos/1");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Todos content = restApi.GetContent<Todos>(response);
            Assert.AreEqual(null, content.UserId);
            Assert.AreEqual(null, content.Id);
            Assert.AreEqual(null, content.Title);
            Assert.AreEqual(null, content.Completed);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }
    }
}
