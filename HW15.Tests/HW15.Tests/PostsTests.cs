﻿using System.Collections.Generic;
using HW15.Tests.DataModels;
using NUnit.Framework;
using RestSharp;

namespace HW15.Tests
{
    [TestFixture]
    public class PostsTests
    {
        [Test]
        public static void GetThirdPost()
        {
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts/3");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Post content = restApi.GetContent<Post>(response);
            Assert.AreEqual("ea molestias quasi exercitationem repellat qui ipsa sit aut", content.Title);
            Assert.AreEqual("et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut", content.Body);
            Assert.AreEqual("1", content.UserId);
            Assert.AreEqual("3", content.Id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void GetAllPosts()
        {
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Post> content = restApi.GetListWithContents<Post>(response);
            int number = content.Count;
            Assert.AreEqual(100, number);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [TestCase("The Best Board Games", "sudak", "1")]
        [TestCase("Board Games", "Misterium", "2")]
        [TestCase("The Best", "WizardsDev", "3")]
        [TestCase("Best Board", "Doska", "4")]
        [TestCase("Games", "DOTA", "5")]
        public static void CreateNewPost(string expTitle, string expBody, string expUserId)
        {
            Dictionary<string, string> body = new Dictionary<string, string>
            {
                {"userId" , expUserId},
                {"body", expBody},
                {"title", expTitle}

            };
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts");
            var restRequest = restApi.CreatePostRequestDictionary(body);
            var response = restApi.GetResponse(restUrl, restRequest);
            Post content = restApi.GetContent<Post>(response);
            Assert.AreEqual(expTitle, content.Title.ToString());
            Assert.AreEqual(expBody, content.Body.ToString());
            Assert.AreEqual(expUserId, content.UserId.ToString());
            Assert.AreEqual("Created", response.StatusCode.ToString());
        }

        [Test]
        public static void UpdateFirstPost()
        {
            string jsonString = @"{
                ""title"": ""The Witcher"",
                ""body"": ""Gwent"",
                ""userId"": ""2"",
                ""id"": ""1""
              }";
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts/1");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Post content = restApi.GetContent<Post>(response);
            Assert.AreEqual("The Witcher", content.Title);
            Assert.AreEqual("Gwent", content.Body);
            Assert.AreEqual("2", content.UserId);
            Assert.AreEqual("1", content.Id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void PatchTitleInFirstPost()
        {
            string jsonString = @"{
                ""title"": ""CD Project The Witcher""
              }";
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts/1");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Post content = restApi.GetContent<Post>(response);
            Assert.AreEqual("CD Project The Witcher", content.Title);
            Assert.AreEqual("quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto", content.Body);
            Assert.AreEqual("1", content.UserId);
            Assert.AreEqual("1", content.Id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void GetAllPostsThatBelongToTheFirstUser()
        {
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts?userId=1");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Post> content = restApi.GetListWithContents<Post>(response);
            Assert.AreEqual("sunt aut facere repellat provident occaecati excepturi optio reprehenderit", content[0].Title);
            Assert.AreEqual("quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto", content[0].Body);
            Assert.AreEqual("1", content[0].UserId);
            Assert.AreEqual("1", content[0].Id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void DeleteFirstPost()
        {
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts/1");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Post content = restApi.GetContent<Post>(response);
            Assert.AreEqual(null, content.Title);
            Assert.AreEqual(null, content.Body);
            Assert.AreEqual(null, content.UserId);
            Assert.AreEqual(null, content.Id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void GetAllCommentsThatBelongToTheFirstPost()
        {
            RestApiHelper<Post> restApi = new RestApiHelper<Post>();
            var restUrl = restApi.SetUrl("/posts/1/comments");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Comments> content = restApi.GetListWithContents<Comments>(response);
            int number = content.Count;
            Assert.AreEqual(5, number);
            Assert.AreEqual("vero eaque aliquid doloribus et culpa", content[4].name);
            Assert.AreEqual("Hayden@althea.biz", content[4].email);
            Assert.AreEqual("harum non quasi et ratione\ntempore iure ex voluptates in ratione\nharum architecto fugit inventore cupiditate\nvoluptates magni quo et", content[4].body);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }
    }
}
