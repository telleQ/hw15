﻿using HW15.Tests.DataModels;
using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;

namespace HW15.Tests
{
    [TestFixture]
    public class UsersTests
    {
        [Test]
        public static void GetEighthUser()
        {
            RestApiHelper<User> restApi = new RestApiHelper<User>();
            var restUrl = restApi.SetUrl("/users/8");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            User content = restApi.GetContent<User>(response);
            
            Assert.AreEqual("8", content.Id);
            Assert.AreEqual("Nicholas Runolfsdottir V", content.Name);
            Assert.AreEqual("Maxime_Nienow", content.Username);
            Assert.AreEqual("Sherwood@rosamond.me", content.Email);
            Assert.AreEqual("586.493.6943 x140", content.Phone);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void AddNewUser()
        {
            string jsonString = @"{
                ""id"": ""11"",
                ""name"": ""Ilya Mihailovskyi"",
                ""username"": ""Ilya.Miha1488"",
                ""email"": ""miha.ilya@i.ua"",
                ""phone"": ""38-067-873-41-72"",
                ""website"": ""hildegard.org""
              }";
            RestApiHelper<User> restApi = new RestApiHelper<User>();
            var restUrl = restApi.SetUrl("/users");
            var restRequest = restApi.CreatePostRequest(jsonString);
            var response = restApi.GetResponse(restUrl, restRequest);
            User content = restApi.GetContent<User>(response);
            Assert.AreEqual("11", content.Id);
            Assert.AreEqual("Ilya Mihailovskyi", content.Name);
            Assert.AreEqual("Ilya.Miha1488", content.Username);
            Assert.AreEqual("miha.ilya@i.ua", content.Email);
            Assert.AreEqual("38-067-873-41-72", content.Phone);
            Assert.AreEqual("hildegard.org", content.Website);
            Assert.AreEqual("Created", response.StatusCode.ToString());
        }

        [Test]
        public static void UpdateFirstUser()
        {
            string jsonString = @"{
                ""id"": ""1"",
                ""name"": ""Leanne Graham"",
                ""username"": ""Test"",
                ""email"": ""Sincere@april.biz"",
                ""phone"": ""1-770-736-8031 x56442"",
                ""website"": ""jsonplaceholder.org""
              }";
            RestApiHelper<User> restApi = new RestApiHelper<User>();
            var restUrl = restApi.SetUrl("/users/1");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            User content = restApi.GetContent<User>(response);
            Assert.AreEqual("1", content.Id);
            Assert.AreEqual("Leanne Graham", content.Name);
            Assert.AreEqual("Test", content.Username);
            Assert.AreEqual("Sincere@april.biz", content.Email);
            Assert.AreEqual("1-770-736-8031 x56442", content.Phone);
            Assert.AreEqual("jsonplaceholder.org", content.Website);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void PatchUserNameInFirstUser()
        {
            string jsonString = @"{
                ""username"": ""Gadza""
              }";
            RestApiHelper<User> restApi = new RestApiHelper<User>();
            var restUrl = restApi.SetUrl("/users/1");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            User content = restApi.GetContent<User>(response);
            Assert.AreEqual("Gadza", content.Username);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void DeleteFirstUser()
        {
            RestApiHelper<User> restApi = new RestApiHelper<User>();
            var restUrl = restApi.SetUrl("/users/1");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            User content = restApi.GetContent<User>(response);
            Assert.AreEqual(null, content.Id);
            Assert.AreEqual(null, content.Name);
            Assert.AreEqual(null, content.Username);
            Assert.AreEqual(null, content.Email);
            Assert.AreEqual(null, content.Phone);
            Assert.AreEqual(null, content.Website);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void GetAllUsers()
        {
            RestApiHelper<User> restApi = new RestApiHelper<User>();
            var restUrl = restApi.SetUrl("/users");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            List<User> content = restApi.GetListWithContents<User>(response);
            int number = content.Count;
            Assert.AreEqual(10, number);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }
    }
}
