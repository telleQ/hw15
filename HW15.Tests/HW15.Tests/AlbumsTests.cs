﻿using NUnit.Framework;
using RestSharp;
using HW15.Tests.DataModels;
using System.Collections.Generic;

namespace HW15.Tests
{
    [TestFixture]
    public class AlbumsTests
    {
        [Test]
        public static void GetThirdAlbum()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/3");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual("1", content.userId);
            Assert.AreEqual("3", content.id);
            Assert.AreEqual("omnis laborum odio", content.title);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [TestCase("555", "101", "natus impedit quibusdam illo est")]
        [TestCase("444", "101", "quibusdam autem aliquid et et quia")]
        [TestCase("333", "101", "qui fuga est a eum")]
        [TestCase("222", "101", "saepe unde necessitatibus rem")]
        [TestCase("111", "101", "distinctio laborum qui")]
        public static void CreateNewAlbums(string expUserId, string expId, string expTitle)
        {
            Dictionary<string, string> body = new Dictionary<string, string>
            {
                {"userId" , expUserId},
                {"id", expId},
                {"title", expTitle}

            };
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums");
            var restRequest = restApi.CreatePostRequestDictionary(body);
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual(expUserId, content.userId.ToString());
            Assert.AreEqual(expId, content.id.ToString());
            Assert.AreEqual(expTitle, content.title.ToString());
            Assert.AreEqual("Created", response.StatusCode.ToString());
        }


        [Test]
        public static void AddAlbum()
        {
            string jsonString = @"{
                ""userId"": ""78"",
                ""id"": ""101"",
                ""title"": ""natus impedit quibusdam illo est""
              }";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums");
            var restRequest = restApi.CreatePostRequest(jsonString);
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual("78", content.userId);
            Assert.AreEqual("101", content.id);
            Assert.AreEqual("natus impedit quibusdam illo est", content.title);
            Assert.AreEqual("Created", response.StatusCode.ToString());
        }

        [Test]
        public static void UpdateFirstAlbums()
        {
            string jsonString = @"{
                ""userId"": ""58"",
                ""id"": ""1"",
                ""title"": ""Best Records""
              }";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/1");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual("58", content.userId);
            Assert.AreEqual("1", content.id);
            Assert.AreEqual("Best Records", content.title);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void PatchTitleInFirstAlbum()
        {
            string jsonString = @"{
                ""title"": ""I love testing""
              }";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/1");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual("I love testing", content.title);
            Assert.AreEqual("1", content.userId);
            Assert.AreEqual("1", content.id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void DeleteFirstAlbum()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/1");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual(null, content.userId);
            Assert.AreEqual(null, content.id);
            Assert.AreEqual(null, content.title);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void GetAllAlbums()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Albums> content = restApi.GetListWithContents<Albums>(response);
            int number = content.Count;
            Assert.AreEqual(100, number);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }
    }
}
