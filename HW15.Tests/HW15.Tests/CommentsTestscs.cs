﻿using System.Collections.Generic;
using HW15.Tests.DataModels;
using NUnit.Framework;
using RestSharp;

namespace HW15.Tests
{
    [TestFixture]
    class CommentsTestscs
    {
        [Test]
        public static void GetSecondComment()
        {
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/2");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);
            Assert.AreEqual("1", content.postId);
            Assert.AreEqual("2", content.id);
            Assert.AreEqual("quo vero reiciendis velit similique earum", content.name);
            Assert.AreEqual("Jayne_Kuhic@sydney.com", content.email);
            Assert.AreEqual("est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et", content.body);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void AddCommentInComments()
        {
            string jsonString = @"{
                ""postId"": ""23"",
                ""id"": ""101"",
                ""name"": ""Comment added one time"",
                ""email"": ""test@i.ua"",
                ""body"": ""Simple dimple, pop it, squish.Pop it, squish, pop it, squish""
              }";
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/albums");
            var restRequest = restApi.CreatePostRequest(jsonString);
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);
            Assert.AreEqual("23", content.postId);
            Assert.AreEqual("101", content.id);
            Assert.AreEqual("Comment added one time", content.name);
            Assert.AreEqual("test@i.ua", content.email);
            Assert.AreEqual("Simple dimple, pop it, squish.Pop it, squish, pop it, squish", content.body);
            Assert.AreEqual("Created", response.StatusCode.ToString());
        }

        [Test]
        public static void UpdateSecondComments()
        {
            string jsonString = @"{
                ""postId"": ""1"",
                ""id"": ""2"",
                ""name"": ""quo vero reiciendis velit similique earum"",
                ""email"": ""Update.test@i.ua"",
                ""body"": ""Simple dimple, pop it, squish.Pop it, squish, pop it, squish""
              }";
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/2");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);
            Assert.AreEqual("1", content.postId);
            Assert.AreEqual("2", content.id);
            Assert.AreEqual("quo vero reiciendis velit similique earum", content.name);
            Assert.AreEqual("Update.test@i.ua", content.email);
            Assert.AreEqual("Simple dimple, pop it, squish.Pop it, squish, pop it, squish", content.body);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void PatchTitleInSecondComments()
        {
            string jsonString = @"{
                ""body"": ""I love testing""
              }";
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/2");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);
            Assert.AreEqual("I love testing", content.body);
            Assert.AreEqual("1", content.postId);
            Assert.AreEqual("2", content.id);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void DeleteSecondComments()
        {
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/2");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);
            Assert.AreEqual(null, content.postId);
            Assert.AreEqual(null, content.id);
            Assert.AreEqual(null, content.name);
            Assert.AreEqual(null, content.email);
            Assert.AreEqual(null, content.body);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }

        [Test]
        public static void GetAllComments()
        {
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Comments> content = restApi.GetListWithContents<Comments>(response);
            int number = content.Count;
            Assert.AreEqual(500, number);
            Assert.AreEqual("OK", response.StatusCode.ToString());
        }
    }
}
